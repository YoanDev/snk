import React, {useState} from 'react'
import Navbar from '../components/Navbar'
import HeroSection from '../components/HeroSection'
import Footer from '../components/Footer'
import Sidebar from '../components/Sidebar'
import HeroSection2 from '../components/HeroSection2'


const Home = () => {
    const [isOpen, setIsOpen] = useState(false)
    const toggle = ()=>{
        setIsOpen(!isOpen)
    }

    return (
        <div>
            <Navbar toggle={toggle} />
            <HeroSection />
            <Footer />
            <Sidebar isOpen={isOpen} toggle={toggle}/>

        </div>
    )
}

export default Home
