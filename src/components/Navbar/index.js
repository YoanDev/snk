import Aos from 'aos'
import 'aos/dist/aos.css'
import React, {useEffect, useState} from 'react'
import { MobileIcon, Nav, NavbarContainer, NavBtn, NavBtnLink, NavItems, NavLinks, NavLogo, NavMenu } from './NavbarElements'
import {FaBars} from 'react-icons/fa'
import {animateScroll as scroll} from 'react-scroll'
import useDocumentScrollThrottled from './useDocumentScrollThrottled';
import {Modal} from '../Form/Modal/modal'
import LogoB from '../../images/logobataillon.png'

const Navbar = ({toggle}) => {

    const [shouldHideHeader, setShouldHideHeader] = useState(false);
    const [shouldShowShadow, setShouldShowShadow] = useState(false);
  
    const MINIMUM_SCROLL = 80;
    const TIMEOUT_DELAY = 400;
  
    useDocumentScrollThrottled(callbackData => {
      const { previousScrollTop, currentScrollTop } = callbackData;
      const isScrolledDown = previousScrollTop < currentScrollTop;
      const isMinimumScrolled = currentScrollTop > MINIMUM_SCROLL;
  
      setShouldShowShadow(currentScrollTop > 2);
  
      setTimeout(() => {
        setShouldHideHeader(isScrolledDown && isMinimumScrolled);
      }, TIMEOUT_DELAY);
    });
  
    const shadowStyle = shouldShowShadow ? 'shadow' : '';
    const hiddenStyle = shouldHideHeader ? 'hidden' : '';
  

    const [showModal, setShowModal] = useState(false)

    const openModal = ()=>{
        setShowModal(prev => !prev)
      }
    

    const toggleHome = ()=>{
        scroll.scrollToTop();
    }

   


    useEffect(()=>{
        Aos.init({duration:2000})
    }, [])
    
    return (
        <>
        <Nav
        className={`header ${shadowStyle} ${hiddenStyle}`}
         >
            <NavbarContainer data-aos='fade-left'>
                <NavLogo to='/' onClick={toggleHome} src={LogoB}/>
                <MobileIcon onClick={toggle}>
                    <FaBars/>
                </MobileIcon>
                <NavMenu>
                    <NavItems>
                        <NavLinks to='/concessionaire'>Story</NavLinks>
                    </NavItems>
                    <NavItems>
                        <NavLinks to='/ecology' smooth={true} duration={500} spy={true} exact='true' offset={-80}>About</NavLinks>
                    </NavItems>
                    
                    
                    
                    
                        
                </NavMenu>

               
            </NavbarContainer>
        </Nav>
        <Modal showModal={showModal} setShowModal={setShowModal}/>
        </>
    )
}

export default Navbar
