import React from 'react'
import CardFirst from './CardFirst'
import CardFour from './CardFirst/CardFour'
import CardSec from './CardFirst/CardSec'
import CardThird from './CardFirst/CardThird'


const Cards = () => {
    return (
        <>
            <CardFirst />
            <CardSec />
            <CardThird />
            <CardFour />
        </>
    )
}

export default Cards
