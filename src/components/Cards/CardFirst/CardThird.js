import React from 'react'
import { Card1, CardContent, CardFirstContainer, CardImg, CardItems, CardText } from './CardFirst'
import CardImage from '../../../images/yelena.jpg'
import CardImage2 from '../../../images/reiner.png'
import CardImage3 from '../../../images/sieg.jpg'
import { motion } from "framer-motion"

const CardThird = () => {
    return (
        <>
        <CardFirstContainer>
            <Card1>
                <CardContent>
                        <CardImg src={CardImage} />
                        <CardText>Armin Arlert</CardText>
                        
                </CardContent>
            </Card1>
            <Card1>
                <CardContent>
                        <CardImg src={CardImage2} />
                        <CardText>Mikasa Ackerman</CardText>
                        
                </CardContent>
            </Card1>
            <Card1>
                <CardContent>
                        <CardImg src={CardImage3} />
                        <CardText>Livai Ackerman</CardText>
                        
                </CardContent>
            </Card1>
        </CardFirstContainer>
            
        </>
    )
}

export default CardThird
