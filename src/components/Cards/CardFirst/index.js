import React from 'react'
import { Card1, CardContent, CardFirstContainer, CardImg, CardItems, CardText } from './CardFirst'
import CardImage from '../../../images/eren-5.jpg'
import CardImage2 from '../../../images/Jean.jpg'
import CardImage3 from '../../../images/sacha.jpg'
import { motion } from "framer-motion"

const CardFirst = () => {
    return (
        <>
        <CardFirstContainer>
            <Card1>
                <CardContent>
                        <CardImg src={CardImage} />
                        <CardText>Eren Jaeger</CardText>
                        
                </CardContent>
            </Card1>
            <Card1>
                <CardContent>
                        <CardImg src={CardImage2} />
                        <CardText>Eren Jaeger</CardText>
                        
                </CardContent>
            </Card1>
            <Card1>
                <CardContent>
                        <CardImg src={CardImage3} />
                        <CardText>Eren Jaeger</CardText>
                        
                </CardContent>
            </Card1>
        </CardFirstContainer>
            
        </>
    )
}

export default CardFirst
