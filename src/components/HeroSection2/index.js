import React, { useState } from 'react'
import BgSnk from '../../images/snk-wallpaper2.jpg'
import Cards from '../Cards'
import { ArrowForward, ArrowRight, HeroBg, HeroContainer, HeroContent, HeroH1, HeroP, VideoBg, HeroBtnWrapper } from './HeroElements2'

const HeroSection2 = () => {
    const [hover, setHover] = useState(false)

    const onHover = () => {
        setHover(!hover)
    }
    return (
        <>
        <HeroContainer>
            <HeroBg>
                <VideoBg src={BgSnk} alt='snk' />
            </HeroBg>
            <HeroContent>
                <HeroH1></HeroH1>
                <HeroP>
    
                </HeroP>
            </HeroContent>

        </HeroContainer>
            <Cards />
            </>

    )
}

export default HeroSection2
