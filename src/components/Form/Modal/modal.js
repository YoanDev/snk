import React, { useRef, useEffect, useCallback } from 'react';
import {useForm} from 'react-hook-form'
import { useSpring, animated } from 'react-spring';
import { ModalInput, ModalLabel, ModalSubmit, ModalSubmitText, CloseModalButton, Background, ModalForm, ModalContent } from './FormElements';



export const Modal = ({ showModal, setShowModal }) => {
  const modalRef = useRef();

  const animation = useSpring({
    config: {
      duration: 250
    },
    opacity: showModal ? 1 : 0,
    transform: showModal ? `translateY(0%)` : `translateY(-100%)`
  });

  const closeModal = e => {
    if (modalRef.current === e.target) {
      setShowModal(false);
    }
  };

  const keyPress = useCallback(
    e => {
      if (e.key === 'Escape' && showModal) {
        setShowModal(false);
        console.log('I pressed');
      }
    },
    [setShowModal, showModal]
  );

  useEffect(
    () => {
      document.addEventListener('keydown', keyPress);
      return () => document.removeEventListener('keydown', keyPress);
    },
    [keyPress]
  );


  const wait = function(duration = 1000){
    return new Promise((resolve) =>{
      window.setTimeout(resolve, duration)
    })
  }


  const {register, handleSubmit, formState, errors} = useForm({
    mode: 'ontouch'
  })


  const {isSubmitting, isValid} = formState


  const onSubmit = async data => {
    await wait(2000)
  }

  console.log(isValid)

  return (
    <>
      {showModal ? (
        <Background onClick={closeModal} ref={modalRef}>
          <animated.div style={animation}>
            <ModalForm showModal={showModal} onSubmit={onSubmit} >
              <ModalContent>
                <ModalLabel for='name' >Username</ModalLabel>
                <ModalInput type='text' id='name' name='user_name' ref={register({required: 'you need to enter a username', minLength:{value:10, message:'need at least 10 characters'} })} />
                {errors.username && <span>{errors.username.message} </span>}
              </ModalContent>
              <ModalContent>
                <ModalLabel for='email' >Email</ModalLabel>
                <ModalInput type='email' id='email' name='email' ref={register({required: true})} />
              </ModalContent>
              <ModalContent>
                <ModalLabel for='password' >Password</ModalLabel>
                <ModalInput type='password' id='password' name='password' ref={register({required: true})} />
              </ModalContent>
              <ModalSubmit disabled={isSubmitting || !isValid}>
                <ModalSubmitText>Submit</ModalSubmitText>
              </ModalSubmit>
              <CloseModalButton
                aria-label='Close modal'
                onClick={() => setShowModal(prev => !prev)}
              />
            </ModalForm>
          </animated.div>
        </Background>
      ) : null}
    </>
  );
};