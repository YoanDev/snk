import './App.css';
import {BrowserRouter as Router, Route, Switch, Link, Redirect} from 'react-router-dom'
import Home from './pages/index';
import Error from './pages/error';

function App() {
  return (
    <>
    <Router>
      <Switch>
        <Route path='/' component={Home} exact />
        
        <Redirect to="/" />
      </Switch>
    </Router>
    </>
  );
}

export default App;
